# Spring Pet Clinic
An implementation of the Spring Pet Clinic application with Spring 5 / Spring Boot 2.

## Modules
* `petclinic-data` - contains the JPA data classes
* `petclinic-web` - the web application