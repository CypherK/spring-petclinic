package ch.cypherk.springpetclinic.dao;

import java.util.Optional;
import java.util.Set;

/**a dao providing access to entities*/
public interface CrudDAO<T, ID> {

    /**stores the entity
     * @return an instance of the entity with the correctly assigned id
     */
    T save(T entity);

    /**@return all known entities*/
    Set<T> findAll();

    /**@return an optional, holding the entity with the given id if one exists*/
    Optional<T> findById(ID id);

    /**delete the entity*/
    void delete(T entity);

    /**delete the entity with the given ID, if one exists*/
    void deleteById(ID id);
}
