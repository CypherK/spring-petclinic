package ch.cypherk.springpetclinic.dao;

import ch.cypherk.springpetclinic.model.person.Owner;

import java.util.Set;

/**A dao providing access to {@link Owner}s*/
public interface OwnerDAO extends CrudDAO<Owner,Long> {

    /**@return all known {@link Owner}s with the given lastName*/
    Set<Owner> findByLastName(String lastName);
}
