package ch.cypherk.springpetclinic.dao;

import ch.cypherk.springpetclinic.model.animal.Pet;

/**A dao providing access to {@link Pet}s*/
public interface PetDAO extends CrudDAO<Pet,Long> {

}
