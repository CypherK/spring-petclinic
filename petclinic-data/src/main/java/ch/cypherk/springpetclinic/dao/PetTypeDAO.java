package ch.cypherk.springpetclinic.dao;

import ch.cypherk.springpetclinic.model.animal.PetType;

public interface PetTypeDAO extends CrudDAO<PetType,Long> {

}
