package ch.cypherk.springpetclinic.dao;

import ch.cypherk.springpetclinic.model.person.Vet;

/**a dao providing access to {@link Vet}s*/
public interface VetDAO extends CrudDAO<Vet,Long> {
}
