package ch.cypherk.springpetclinic.dao;

import ch.cypherk.springpetclinic.model.person.VetSpecialty;

public interface VetSpecialtyDAO extends CrudDAO<VetSpecialty,Long> {
}
