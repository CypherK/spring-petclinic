package ch.cypherk.springpetclinic.dao;

import ch.cypherk.springpetclinic.model.clinic.Visit;

public interface VisitDAO extends CrudDAO<Visit,Long> {
}
