package ch.cypherk.springpetclinic.dao.jpa;

import ch.cypherk.springpetclinic.dao.OwnerDAO;
import ch.cypherk.springpetclinic.model.person.Owner;
import ch.cypherk.springpetclinic.repository.OwnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Repository
@Profile("jpa")
public class OwnerJpaDAO implements OwnerDAO {

    private OwnerRepository ownerRepository;

    @Autowired
    public OwnerJpaDAO(OwnerRepository ownerRepository) {
        this.ownerRepository = ownerRepository;
    }

    /**
     * @return all known {@link Owner}s with the given lastName
     */
    @Override
    public Set<Owner> findByLastName(String lastName) {
        return new HashSet<>(ownerRepository.findByLastName(lastName));
    }

    /**
     * stores the entity
     * @return an instance of the entity with the correctly assigned id
     */
    @Override
    public Owner save(Owner entity) {
        return ownerRepository.save(entity);
    }

    /**
     * @return all known entities
     */
    @Override
    public Set<Owner> findAll() {
        return new HashSet<>((Collection<? extends Owner>) ownerRepository.findAll());
    }

    /**
     * @return an optional, holding the entity with the given id if one exists
     */
    @Override
    public Optional<Owner> findById(Long id) {
        return ownerRepository.findById(id);
    }

    /**
     * delete the entity
     */
    @Override
    public void delete(Owner entity) {
        ownerRepository.delete(entity);

    }

    /**
     * delete the entity with the given ID, if one exists
     */
    @Override
    public void deleteById(Long id) {
        ownerRepository.deleteById(id);

    }
}
