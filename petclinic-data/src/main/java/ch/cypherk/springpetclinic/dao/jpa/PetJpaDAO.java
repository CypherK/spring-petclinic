package ch.cypherk.springpetclinic.dao.jpa;

import ch.cypherk.springpetclinic.dao.PetDAO;
import ch.cypherk.springpetclinic.model.animal.Pet;
import ch.cypherk.springpetclinic.repository.PetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Repository
@Profile("jpa")
public class PetJpaDAO implements PetDAO {

    private PetRepository petRepository;

    @Autowired
    public PetJpaDAO(PetRepository petRepository) {
        this.petRepository = petRepository;
    }

    /**
     * stores the entity
     * @return an instance of the entity with the correctly assigned id
     */
    @Override
    public Pet save(Pet entity) {
        return petRepository.save(entity);
    }

    /**
     * @return all known entities
     */
    @Override
    public Set<Pet> findAll() {
        return new HashSet<>((Collection<? extends Pet>) petRepository.findAll());
    }

    /**
     * @return an optional, holding the entity with the given id if one exists
     */
    @Override
    public Optional<Pet> findById(Long id) {
        return petRepository.findById(id);
    }

    /**
     * delete the entity
     */
    @Override
    public void delete(Pet entity) {
        petRepository.delete(entity);
    }

    /**
     * delete the entity with the given ID, if one exists
     */
    @Override
    public void deleteById(Long id) {
        petRepository.deleteById(id);
    }
}
