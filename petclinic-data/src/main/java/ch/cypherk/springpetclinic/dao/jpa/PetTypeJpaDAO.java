package ch.cypherk.springpetclinic.dao.jpa;

import ch.cypherk.springpetclinic.dao.PetTypeDAO;
import ch.cypherk.springpetclinic.model.animal.PetType;
import ch.cypherk.springpetclinic.repository.PetTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Repository
@Profile("jpa")
public class PetTypeJpaDAO implements PetTypeDAO {

    private PetTypeRepository petTypeRepository;

    @Autowired
    public PetTypeJpaDAO(PetTypeRepository petTypeRepository) {
        this.petTypeRepository = petTypeRepository;
    }

    /**
     * stores the entity
     * @return an instance of the entity with the correctly assigned id
     */
    @Override
    public PetType save(PetType entity) {
        return petTypeRepository.save(entity);
    }

    /**
     * @return all known entities
     */
    @Override
    public Set<PetType> findAll() {
        return new HashSet<>((Collection<? extends PetType>) petTypeRepository.findAll());
    }

    /**
     * @return an optional, holding the entity with the given id if one exists
     */
    @Override
    public Optional<PetType> findById(Long id) {
        return petTypeRepository.findById(id);
    }

    /**
     * delete the entity
     */
    @Override
    public void delete(PetType entity) {
        petTypeRepository.delete(entity);
    }

    /**
     * delete the entity with the given ID, if one exists
     */
    @Override
    public void deleteById(Long id) {
        petTypeRepository.deleteById(id);
    }
}
