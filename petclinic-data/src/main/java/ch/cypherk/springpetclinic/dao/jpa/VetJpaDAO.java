package ch.cypherk.springpetclinic.dao.jpa;

import ch.cypherk.springpetclinic.dao.VetDAO;
import ch.cypherk.springpetclinic.model.person.Vet;
import ch.cypherk.springpetclinic.repository.VetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Repository
@Profile("jpa")
public class VetJpaDAO implements VetDAO {

    private VetRepository vetRepository;

    @Autowired
    public VetJpaDAO(VetRepository vetRepository) {
        this.vetRepository = vetRepository;
    }

    /**
     * stores the entity
     * @return an instance of the entity with the correctly assigned id
     */
    @Override
    public Vet save(Vet entity) {
        return vetRepository.save(entity);
    }

    /**
     * @return all known entities
     */
    @Override
    public Set<Vet> findAll() {
        return new HashSet<>((Collection<? extends Vet>) vetRepository.findAll());
    }

    /**
     * @return an optional, holding the entity with the given id if one exists
     */
    @Override
    public Optional<Vet> findById(Long id) {
        return vetRepository.findById(id);
    }

    /**
     * delete the entity
     */
    @Override
    public void delete(Vet entity) {
        vetRepository.delete(entity);
    }

    /**
     * delete the entity with the given ID, if one exists
     */
    @Override
    public void deleteById(Long id) {
        vetRepository.deleteById(id);
    }
}
