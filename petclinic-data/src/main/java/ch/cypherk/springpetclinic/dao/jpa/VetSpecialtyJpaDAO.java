package ch.cypherk.springpetclinic.dao.jpa;

import ch.cypherk.springpetclinic.dao.VetSpecialtyDAO;
import ch.cypherk.springpetclinic.model.person.VetSpecialty;
import ch.cypherk.springpetclinic.repository.VetSpecialtyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Repository
@Profile("jpa")
public class VetSpecialtyJpaDAO implements VetSpecialtyDAO {

    private VetSpecialtyRepository vetSpecialtyRepository;

    @Autowired
    public VetSpecialtyJpaDAO(VetSpecialtyRepository vetSpecialtyRepository) {
        this.vetSpecialtyRepository = vetSpecialtyRepository;
    }

    /**
     * stores the entity
     * @return an instance of the entity with the correctly assigned id
     */
    @Override
    public VetSpecialty save(VetSpecialty entity) {
        return vetSpecialtyRepository.save(entity);
    }

    /**
     * @return all known entities
     */
    @Override
    public Set<VetSpecialty> findAll() {
        return new HashSet<>((Collection<? extends VetSpecialty>) vetSpecialtyRepository.findAll());
    }

    /**
     * @return an optional, holding the entity with the given id if one exists
     */
    @Override
    public Optional<VetSpecialty> findById(Long id) {
        return vetSpecialtyRepository.findById(id);
    }

    /**
     * delete the entity
     */
    @Override
    public void delete(VetSpecialty entity) {
        vetSpecialtyRepository.delete(entity);
    }

    /**
     * delete the entity with the given ID, if one exists
     */
    @Override
    public void deleteById(Long id) {
        vetSpecialtyRepository.deleteById(id);
    }
}
