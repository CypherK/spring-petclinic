package ch.cypherk.springpetclinic.dao.jpa;

import ch.cypherk.springpetclinic.dao.VisitDAO;
import ch.cypherk.springpetclinic.model.clinic.Visit;
import ch.cypherk.springpetclinic.repository.VisitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Repository
@Profile("jpa")
public class VisitJpaDAO implements VisitDAO {

    private VisitRepository visitRepository;

    @Autowired
    public VisitJpaDAO(VisitRepository visitRepository) {
        this.visitRepository = visitRepository;
    }

    /**
     * stores the entity
     * @return an instance of the entity with the correctly assigned id
     */
    @Override
    public Visit save(Visit entity) {
        return visitRepository.save(entity);
    }

    /**
     * @return all known entities
     */
    @Override
    public Set<Visit> findAll() {
        return new HashSet<>((Collection<? extends Visit>) visitRepository.findAll());
    }

    /**
     * @return an optional, holding the entity with the given id if one exists
     */
    @Override
    public Optional<Visit> findById(Long id) {
        return visitRepository.findById(id);
    }

    /**
     * delete the entity
     */
    @Override
    public void delete(Visit entity) {
        visitRepository.delete(entity);
    }

    /**
     * delete the entity with the given ID, if one exists
     */
    @Override
    public void deleteById(Long id) {
        visitRepository.deleteById(id);
    }
}
