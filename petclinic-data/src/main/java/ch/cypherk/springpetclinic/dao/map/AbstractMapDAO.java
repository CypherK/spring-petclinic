package ch.cypherk.springpetclinic.dao.map;

import ch.cypherk.springpetclinic.dao.CrudDAO;
import ch.cypherk.springpetclinic.model.BaseEntity;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

abstract class AbstractMapDAO<T extends BaseEntity> implements CrudDAO<T,Long> {
    protected Map<Long,T> map = new HashMap<>();

    private AtomicLong nextID = new AtomicLong(1L);

    /**stores the entity*/
    @Override
    public T save(T entity) {
        if(entity == null) throw new IllegalArgumentException("cannot save null entity");

        Long myID = entity.getId();
        if(myID == null){
            myID = nextID.getAndIncrement();
            entity.setId(myID);
        }
        map.put(myID,entity);
        return entity;
    }

    /**@return all known entities*/
    @Override
    public Set<T> findAll() {
        return new HashSet<>(map.values());
    }

    /**@return an optional, holding the entity with the given id if one exists*/
    @Override
    public Optional<T> findById(Long id) {
        T candidate = map.get(id);
        if(candidate == null) return Optional.empty();
        else return Optional.of(candidate);
    }

    /**delete the entity*/
    @Override
    public void delete(T entity) {
        Optional<Long> myID = Optional.ofNullable(entity.getId());
        myID.ifPresent(map::remove);
    }

    /**delete the entity with the given ID, if one exists*/
    @Override
    public void deleteById(Long id) {
        if(id == null) throw new IllegalArgumentException("id must not be null");
        map.remove(id);
    }
}
