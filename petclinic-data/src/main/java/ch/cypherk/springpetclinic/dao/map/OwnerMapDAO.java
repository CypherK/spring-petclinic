package ch.cypherk.springpetclinic.dao.map;

import ch.cypherk.springpetclinic.dao.PetDAO;
import ch.cypherk.springpetclinic.model.animal.Pet;
import ch.cypherk.springpetclinic.model.person.Owner;
import ch.cypherk.springpetclinic.dao.OwnerDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
@Profile({"map","default"})
public class OwnerMapDAO extends AbstractMapDAO<Owner> implements OwnerDAO {

    PetDAO petDAO;

    @Autowired
    public OwnerMapDAO(PetDAO petDAO) {
        this.petDAO = petDAO;
    }

    /**
     * stores the entity
     */
    @Override
    public Owner save(Owner owner) {
        Objects.requireNonNull(owner, "cannot save null owner");
        Objects.requireNonNull(owner.getPets(), "pets must have been initialised");
        owner.getPets().forEach(pet -> savePetIfNecessary(owner,pet));
        return super.save(owner);
    }


    private void savePetIfNecessary(Owner owner, Pet pet){
        if(pet.getId() == null){
            Set<Pet> pets = owner.getPets();
            Pet savedPet = petDAO.save(pet);
            pets.remove(pet);
            pets.add(savedPet);
        }
    }

    /**
     * @return all known {@link Owner}s with the given lastName
     */
    @Override
    public Set<Owner> findByLastName(String lastName) {
        return map.values().stream()
                .filter(owner -> hasLastName(owner,lastName))
                .collect(Collectors.toSet())
                ;
    }

    private boolean hasLastName(Owner owner, String expectedLastName){
        if(owner.getLastName() == null) return false;
        else return owner.getLastName().equals(expectedLastName);
    }
}
