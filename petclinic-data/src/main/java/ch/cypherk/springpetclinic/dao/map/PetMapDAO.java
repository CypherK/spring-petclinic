package ch.cypherk.springpetclinic.dao.map;

import ch.cypherk.springpetclinic.dao.PetTypeDAO;
import ch.cypherk.springpetclinic.model.animal.Pet;
import ch.cypherk.springpetclinic.dao.PetDAO;
import ch.cypherk.springpetclinic.model.animal.PetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Objects;

@Repository
@Profile({"map","default"})
public class PetMapDAO extends AbstractMapDAO<Pet> implements PetDAO {

    PetTypeDAO petTypeDAO;

    @Autowired
    public PetMapDAO(PetTypeDAO petTypeDAO) {
        this.petTypeDAO = petTypeDAO;
    }

    /**
     * stores the entity
     */
    @Override
    public Pet save(Pet pet) {
        Objects.requireNonNull(pet, "cannot save null pet");
        Objects.requireNonNull(pet.getName(), "every pet must have a name");
        Objects.requireNonNull(pet.getPetType(),"every pet must have a type");

        PetType petType = pet.getPetType();
        if(petType.getId() == null){
            PetType savedPetType = petTypeDAO.save(petType);
            pet.setPetType(savedPetType);
        }

        return super.save(pet);

    }
}
