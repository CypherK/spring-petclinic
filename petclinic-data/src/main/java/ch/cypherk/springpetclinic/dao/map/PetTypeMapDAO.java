package ch.cypherk.springpetclinic.dao.map;

import ch.cypherk.springpetclinic.dao.PetTypeDAO;
import ch.cypherk.springpetclinic.model.animal.PetType;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Objects;

@Repository
@Profile({"map","default"})
public class PetTypeMapDAO extends AbstractMapDAO<PetType> implements PetTypeDAO {
    /**
     * stores the entity
     */
    @Override
    public PetType save(PetType entity) {
        Objects.requireNonNull(entity, "cannot save null pet type");
        Objects.requireNonNull(entity.getName(), "every PetType must have a name");
        return super.save(entity);
    }
}
