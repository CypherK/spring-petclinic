package ch.cypherk.springpetclinic.dao.map;

import ch.cypherk.springpetclinic.dao.VetSpecialtyDAO;
import ch.cypherk.springpetclinic.model.person.Vet;
import ch.cypherk.springpetclinic.dao.VetDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Objects;

@Repository
@Profile({"map","default"})
public class VetMapDAO extends AbstractMapDAO<Vet> implements VetDAO {

    private VetSpecialtyDAO vetSpecialtyDAO;

    @Autowired
    public VetMapDAO(VetSpecialtyDAO vetSpecialtyDAO) {
        this.vetSpecialtyDAO = vetSpecialtyDAO;
    }

    /**
     * stores the entity
     */
    @Override
    public Vet save(Vet entity) {
        Objects.requireNonNull(entity.getFirstName(),"every vet must have a first name");
        Objects.requireNonNull(entity.getLastName(), "every vet must have a last name");
        entity.getSpecialties().forEach(vetSpecialtyDAO::save);
        return super.save(entity);
    }
}
