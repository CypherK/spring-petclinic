package ch.cypherk.springpetclinic.dao.map;

import ch.cypherk.springpetclinic.dao.VetSpecialtyDAO;
import ch.cypherk.springpetclinic.model.person.VetSpecialty;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Objects;

@Repository
@Profile({"map","default"})
public class VetSpecialtyMapDAO extends AbstractMapDAO<VetSpecialty> implements VetSpecialtyDAO {

    /**
     * stores the entity
     */
    @Override
    public VetSpecialty save(VetSpecialty entity) {
        Objects.requireNonNull(entity.getDescription(),"every vet specialty must have a description");
        return super.save(entity);
    }
}
