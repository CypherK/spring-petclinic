package ch.cypherk.springpetclinic.dao.map;

import ch.cypherk.springpetclinic.dao.VisitDAO;
import ch.cypherk.springpetclinic.model.clinic.Visit;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Objects;

@Repository
@Profile({"map","default"})
public class VisitMapDAO extends AbstractMapDAO<Visit> implements VisitDAO {
    /**
     * stores the entity
     */
    @Override
    public Visit save(Visit entity) {
        Objects.requireNonNull(entity.getPet(), "a visit must have a pet");
        Objects.requireNonNull(entity.getPet().getOwner(), "a visiting pet must have an owner");
        Objects.requireNonNull(entity.getPet().getId(), "a visiting pet must have been persisted");
        Objects.requireNonNull(entity.getPet().getOwner().getId(), "a visiting pet's owner must have been persisted");

        return super.save(entity);
    }
}
