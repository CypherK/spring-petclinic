package ch.cypherk.springpetclinic.model.person;

import ch.cypherk.springpetclinic.model.animal.Pet;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "owners")
public class Owner extends Person {

    @Builder
    public Owner(Long id, String firstName, String lastName, String address, String city, String telephone, Set<Pet> pets) {
        super(id, firstName, lastName);
        this.address = address;
        this.city = city;
        this.telephone = telephone;
        if(pets == null) this.pets = new HashSet<>();
        else this.pets = pets;
    }

    @Column(name = "address")
    private String address;

    @Column(name = "ciry")
    private String city;

    @Column(name = "telephone")
    private String telephone;

    /**pets belonging to this owner*/
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "owner")
    private Set<Pet> pets = new HashSet<>();

    @Override
    public String toString() {
        return "Owner{" +
                "id=" + this.getId() +
                ", firstName=" + this.getFirstName() +
                ", lastName=" + this.getLastName() +
                "}";
    }
}
