package ch.cypherk.springpetclinic.repository;

import ch.cypherk.springpetclinic.model.person.Owner;
import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

@Profile("jpa")
public interface OwnerRepository extends CrudRepository<Owner,Long> {
    Set<Owner> findByLastName(String lastName);
}
