package ch.cypherk.springpetclinic.repository;

import ch.cypherk.springpetclinic.dao.PetDAO;
import ch.cypherk.springpetclinic.model.animal.Pet;
import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.CrudRepository;

@Profile("jpa")
public interface PetRepository extends CrudRepository<Pet,Long> {
}
