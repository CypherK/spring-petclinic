package ch.cypherk.springpetclinic.repository;

import ch.cypherk.springpetclinic.model.animal.PetType;
import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.CrudRepository;

@Profile("jpa")
public interface PetTypeRepository extends CrudRepository<PetType,Long> {
}
