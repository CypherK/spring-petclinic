package ch.cypherk.springpetclinic.repository;

import ch.cypherk.springpetclinic.dao.VetDAO;
import ch.cypherk.springpetclinic.model.person.Vet;
import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.CrudRepository;

@Profile("jpa")
public interface VetRepository extends CrudRepository<Vet,Long> {
}
