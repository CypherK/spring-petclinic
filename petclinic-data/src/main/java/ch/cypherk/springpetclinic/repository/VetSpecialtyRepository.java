package ch.cypherk.springpetclinic.repository;

import ch.cypherk.springpetclinic.dao.VetSpecialtyDAO;
import ch.cypherk.springpetclinic.model.person.VetSpecialty;
import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.CrudRepository;

@Profile("jpa")
public interface VetSpecialtyRepository extends CrudRepository<VetSpecialty,Long> {
}
