package ch.cypherk.springpetclinic.repository;

import ch.cypherk.springpetclinic.dao.VisitDAO;
import ch.cypherk.springpetclinic.model.clinic.Visit;
import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.CrudRepository;

@Profile("jpa")
public interface VisitRepository extends CrudRepository<Visit,Long> {
}
