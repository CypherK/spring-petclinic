package ch.cypherk.springpetclinic.dao.map;

import ch.cypherk.springpetclinic.dao.PetDAO;
import ch.cypherk.springpetclinic.dao.PetTypeDAO;
import ch.cypherk.springpetclinic.model.person.Owner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.Set;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;
import static org.junit.jupiter.api.Assertions.*;

class OwnerMapDAOTest {

    OwnerMapDAO uut;

    private Long ownerID = 1L;
    private String ownerLastName = "Smith";

    @BeforeEach
    void setUp() { // basically "dependency injection"
        PetTypeDAO petTypeDAO = new PetTypeMapDAO();
        PetDAO petDAO = new PetMapDAO(petTypeDAO);
        uut = new OwnerMapDAO(petDAO);

        uut.save(Owner.builder().id(ownerID).lastName(ownerLastName).build());
    }

    @Test
    void findAll() {
        //given

        //when
        Set<Owner> owners = uut.findAll();

        //then
        assertThat(owners.size(),is(1));
    }

    @Test
    void findById() {
        //given

        //when
        Optional<Owner> owner = uut.findById(ownerID);

        //then
        assert(owner.isPresent());
        assertThat(owner.get().getId(),is(ownerID));
    }

    @Test
    void saveExistingID() {
        //given
        Owner owner2 = Owner.builder().id(2L).build();

        //when
        Owner savedOwner = uut.save(owner2);

        //then
        assertThat(savedOwner,notNullValue());
        assertThat(savedOwner.getId(),is(owner2.getId()));
    }

    @Test
    void saveNoID(){
        //given
        Owner owner3 = Owner.builder().build();

        //when
        Owner savedOwner = uut.save(owner3);

        //then
        assertThat(savedOwner,notNullValue());
        assertThat(savedOwner.getId(),notNullValue());
    }

    @Test
    void delete() {
        //given

        //when
        uut.delete(uut.findById(ownerID).get());

        //then
        assertThat(uut.findAll().size(),is(0));

    }

    @Test
    void deleteById() {
        //given

        //when
        uut.deleteById(ownerID);

        //then
        assertThat(uut.findAll().size(),is(0));

    }

    @Test
    void findByLastName() {
        //given

        //when
        Set<Owner> owners = uut.findByLastName(ownerLastName);

        //then
        assertThat(owners.size(),is(1));
    }

    @Test
    void findByLastNameFailing(){
        //given

        //when
        Set<Owner> owners = uut.findByLastName("foo");

        //then
        assertThat(owners.size(),is(0));
    }
}