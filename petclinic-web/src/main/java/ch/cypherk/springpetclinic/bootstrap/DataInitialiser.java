package ch.cypherk.springpetclinic.bootstrap;

import ch.cypherk.springpetclinic.model.animal.Pet;
import ch.cypherk.springpetclinic.model.animal.PetType;
import ch.cypherk.springpetclinic.model.clinic.Visit;
import ch.cypherk.springpetclinic.model.person.Owner;
import ch.cypherk.springpetclinic.model.person.Vet;
import ch.cypherk.springpetclinic.model.person.VetSpecialty;
import ch.cypherk.springpetclinic.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Component
public class DataInitialiser implements CommandLineRunner {

    private final OwnerService ownerService;
    private final VetService vetService;
    private final VetSpecialtyService vetSpecialtyService;
    private final PetService petService;
    private final PetTypeService petTypeService;
    private final VisitService visitService;

    @Autowired
    public DataInitialiser(
            OwnerService ownerService,
            VetService vetService,
            VetSpecialtyService vetSpecialtyService,
            PetService petService,
            PetTypeService petTypeService,
            VisitService visitService
    ){
        this.ownerService = ownerService;
        this.vetService = vetService;
        this.vetSpecialtyService = vetSpecialtyService;
        this.petService = petService;
        this.petTypeService = petTypeService;
        this.visitService = visitService;
    }

    @Override
    public void run(String... args) throws Exception {
        int count = petTypeService.findAll().size();
        if(count == 0) {
            loadData();
        }
    }

    /**populates the database*/
    private void loadData() {
        PetType owl = newPetType("Owl");
        PetType snake = newPetType("Snake");
        PetType cat = newPetType("Cat");
        System.out.println("loaded pet types...");

        Pet hedwig = newPet("Hedwig", owl);
        Pet nagini = newPet("Nagini",snake);
        Pet crookshanks = newPet("Crookshanks",cat);
        System.out.println("loaded pets...");

        Owner harry = newOwner(
                "Harry","Potter",
                "4. Privet Drive",
                "Little Whinging",
                "123-456-78-99"
        );
        hedwig.setOwner(harry);
        harry.getPets().add(hedwig);
        petService.save(hedwig);
        ownerService.save(harry);

        Owner hermione = newOwner(
                "Hermione","Granger",
                "Granger's Street",
                "Heathgate",
                "998-765-43-21"
        );
        crookshanks.setOwner(hermione);
        hermione.getPets().add(crookshanks);
        petService.save(crookshanks);
        ownerService.save(hermione);

        Owner tom = newOwner(
                "Tom", "Riddle",
                "Somewhere In The Dark",
                "London",
                "323-535-22-11"
        );
        nagini.setOwner(tom);
        tom.getPets().add(nagini);
        petService.save(nagini);
        ownerService.save(tom);
        System.out.println("loaded owners...");

        VetSpecialty radiology = newVetSpecialty("radiology");
        VetSpecialty surgery = newVetSpecialty("surgery");
        VetSpecialty dentistry = newVetSpecialty("dentistry");
        System.out.println("loaded vet specialties ...");

        Vet dolittle = newVet(
                "John","Dolittle",
                new HashSet<>(Arrays.asList(dentistry,radiology))
        );
        Vet herriot = newVet(
                "James","Herriot",
                Collections.singleton(surgery)
        );
        System.out.println("loaded vets...");


        Visit hedwigVisit = newVisit(
                hedwig,
                "some ruffled feathers"
        );

        Visit naginiVisit = newVisit(
                nagini,
                "stomach ache from having eaten too many muggles"
        );

        System.out.println("loaded visits...");
    }

    /**prints the content of the DB for confirmation purposes*/
    private void printWholeDB(){
        System.out.println("owners:");
        ownerService.findAll().forEach(System.out::println);

        System.out.println("vets:");
        vetService.findAll().forEach(System.out::println);
    }

    /**creates and inserts the new {@link Owner}
     * @return the created {@link Owner}*/
    private Owner newOwner(
            String firstName,
            String lastName,
            String address,
            String city,
            String telphone
    ){
        Owner owner = new Owner();
        owner.setFirstName(firstName);
        owner.setLastName(lastName);
        owner.setAddress(address);
        owner.setCity(city);
        owner.setTelephone(telphone);
        return ownerService.save(owner);
    }

    /**creates and inserts the new {@link VetSpecialty}
     * @return  the created {@link VetSpecialty}*/
    private VetSpecialty newVetSpecialty(String description){
        VetSpecialty specialty = new VetSpecialty();
        specialty.setDescription(description);
        return vetSpecialtyService.save(specialty);
    }

    /**creates and inserts the new {@link Vet}
     * @return the created {@link Vet}*/
    private Vet newVet(
            String firstName,
            String lastName,
            Set<VetSpecialty> specialties
    ){
        Vet vet = new Vet();
        vet.setFirstName(firstName);
        vet.setLastName(lastName);
        vet.getSpecialties().addAll(specialties);
        return vetService.save(vet);
    }

    /**creates and inserts the new {@link PetType}
     * @return the created {@link PetType}*/
    private PetType newPetType(String name){
        PetType petType = new PetType();
        petType.setName(name);
        return petTypeService.save(petType);
    }

    /**creates and inserts the new {@link Pet}*/
    private Pet newPet(
            String name,
            PetType petType
    ){
        Pet pet = new Pet();
        pet.setName(name);
        pet.setPetType(petType);
        pet.setBirthDate(LocalDate.now());
        return petService.save(pet);
    }

    /**creates and inserts the new {@link Visit}
     * @return the created {@link Visit}*/
    private Visit newVisit(
            Pet pet,
            String description
    ){
        Visit v = new Visit();
        v.setPet(pet);
        v.setDescription(description);
        v.setDate(LocalDate.now());
        return visitService.save(v);
    }


}
