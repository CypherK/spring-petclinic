package ch.cypherk.springpetclinic.service;

import ch.cypherk.springpetclinic.dao.OwnerDAO;
import ch.cypherk.springpetclinic.model.person.Owner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class OwnerService {

    private final OwnerDAO ownerDAO;

    @Autowired
    public OwnerService(OwnerDAO ownerDAO) {
        this.ownerDAO = ownerDAO;
    }

    /**
     * @return all known {@link Owner}s with the given lastName
     */
    public Set<Owner> findByLastName(String lastName) {
        return ownerDAO.findByLastName(lastName);
    }

    /**
     * stores the owner
     * @return an instance of the {@link Owner} with the correctly assigned id
     */
    public Owner save(Owner owner) {
        return ownerDAO.save(owner);
    }

    /**
     * @return all known owners
     */
    public Set<Owner> findAll() {
        return ownerDAO.findAll();
    }

    /**
     * @return an optional, holding the owner with the given id if one exists
     */
    public Optional<Owner> findByID(Long id) {
        return ownerDAO.findById(id);
    }

    /**
     * delete the owner
     */
    public void delete(Owner owner) {
        ownerDAO.delete(owner);
    }

    /**
     * delete the owner with the given ID, if one exists
     */
    public void deleteByID(Long id) {
        ownerDAO.deleteById(id);
    }
}
