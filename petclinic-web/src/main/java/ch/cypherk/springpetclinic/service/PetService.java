package ch.cypherk.springpetclinic.service;

import ch.cypherk.springpetclinic.dao.PetDAO;
import ch.cypherk.springpetclinic.model.animal.Pet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class PetService{
    
    private PetDAO petDAO;

    @Autowired
    public PetService(PetDAO petDAO) {
        this.petDAO = petDAO;
    }

    /**
     * stores the pet
     */
    public Pet save(Pet pet) {
        return petDAO.save(pet);
    }

    /**
     * @return all known pets
     */
    public Set<Pet> findAll() {
        return petDAO.findAll();
    }

    /**
     * @return an optional, holding the pet with the given id if one exists
     */
    public Optional<Pet> findByID(Long id) {
        return petDAO.findById(id);
    }

    /**
     * delete the pet
     */
    public void delete(Pet pet) {
        petDAO.delete(pet);
    }

    /**
     * delete the pet with the given ID, if one exists
     */
    public void deleteByID(Long id) {
        petDAO.deleteById(id);
    }
}
