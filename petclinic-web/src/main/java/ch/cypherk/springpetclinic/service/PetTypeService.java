package ch.cypherk.springpetclinic.service;

import ch.cypherk.springpetclinic.dao.PetTypeDAO;
import ch.cypherk.springpetclinic.model.animal.PetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class PetTypeService{

    private PetTypeDAO petTypeDAO;

    @Autowired
    public PetTypeService(PetTypeDAO petTypeDAO) {
        this.petTypeDAO = petTypeDAO;
    }

    /**
     * stores the entity
     * */
    public PetType save(PetType entity) {
        return petTypeDAO.save(entity);
    }

    /**
     * @return all known entities
     */
    public Set<PetType> findAll() {
        return petTypeDAO.findAll();
    }

    /**
     * @return an optional, holding the entity with the given id if one exists
     */
    public Optional<PetType> findByID(Long id) {
        return petTypeDAO.findById(id);
    }

    /**
     * delete the entity
     */
    public void delete(PetType entity) {
        petTypeDAO.delete(entity);
    }

    /**
     * delete the entity with the given ID, if one exists
     */
    public void deleteByID(Long id) {
        petTypeDAO.deleteById(id);
    }
}
