package ch.cypherk.springpetclinic.service;

import ch.cypherk.springpetclinic.dao.VetDAO;
import ch.cypherk.springpetclinic.model.person.Vet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class VetService {
    
    private VetDAO vetDAO;

    @Autowired
    public VetService(VetDAO vetDAO) {
        this.vetDAO = vetDAO;
    }

    /**
     * stores the vet
     * @return an instance of the {@link Vet} with the correctly assigned id
     */
    public Vet save(Vet vet) {
        return vetDAO.save(vet);
    }

    /**
     * @return all known vets
     */
    public Set<Vet> findAll() {
        return vetDAO.findAll();
    }

    /**
     * @return an optional, holding the vet with the given id if one exists
     */
    public Optional<Vet> findByID(Long id) {
        return vetDAO.findById(id);
    }

    /**
     * delete the vet
     */
    public void delete(Vet vet) {
        vetDAO.delete(vet);
    }

    /**
     * delete the vet with the given ID, if one exists
     */
    public void deleteByID(Long id) {
        vetDAO.deleteById(id);
    }
}
