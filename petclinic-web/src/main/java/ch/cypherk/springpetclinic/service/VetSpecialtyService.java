package ch.cypherk.springpetclinic.service;

import ch.cypherk.springpetclinic.dao.VetSpecialtyDAO;
import ch.cypherk.springpetclinic.model.person.VetSpecialty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class VetSpecialtyService{

    private final VetSpecialtyDAO vetSpecialtyDAO;

    @Autowired
    public VetSpecialtyService(VetSpecialtyDAO vetSpecialtyDAO) {
        this.vetSpecialtyDAO = vetSpecialtyDAO;
    }

    /**
     * stores the entity
     */
    public VetSpecialty save(VetSpecialty entity) {
        return vetSpecialtyDAO.save(entity);
    }

    /**
     * @return all known entities
     */
    public Set<VetSpecialty> findAll() {
        return vetSpecialtyDAO.findAll();
    }

    /**
     * @return an optional, holding the entity with the given id if one exists
     */
    public Optional<VetSpecialty> findByID(Long id) {
        return vetSpecialtyDAO.findById(id);
    }

    /**
     * delete the entity
     */
    public void delete(VetSpecialty entity) {
        vetSpecialtyDAO.delete(entity);
    }

    /**
     * delete the entity with the given ID, if one exists
     */
    public void deleteByID(Long id) {
        vetSpecialtyDAO.deleteById(id);
    }
}
