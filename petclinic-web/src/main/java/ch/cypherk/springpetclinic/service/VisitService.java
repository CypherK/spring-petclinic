package ch.cypherk.springpetclinic.service;

import ch.cypherk.springpetclinic.dao.VisitDAO;
import ch.cypherk.springpetclinic.model.clinic.Visit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class VisitService{

    private final VisitDAO visitDAO;

    @Autowired
    public VisitService(VisitDAO visitDAO) {
        this.visitDAO = visitDAO;
    }

    /**
     * stores the entity
     * @return an instance of the entity with the correctly assigned id
     */
    public Visit save(Visit entity) {
        return visitDAO.save(entity);
    }

    /**
     * @return all known entities
     */
    public Set<Visit> findAll() {
        return visitDAO.findAll();
    }

    /**
     * @return an optional, holding the entity with the given id if one exists
     */
    public Optional<Visit> findByID(Long id) {
        return visitDAO.findById(id);
    }

    /**
     * delete the entity
     */
    public void delete(Visit entity) {
        visitDAO.delete(entity);
    }

    /**
     * delete the entity with the given ID, if one exists
     */
    public void deleteByID(Long id) {
        visitDAO.deleteById(id);
    }
}
